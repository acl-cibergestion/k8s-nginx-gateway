FROM nginx:latest
RUN mkdir -p /usr/share/nginx/html/.well-known/pki-validation

RUN mkdir -p /etc/ssl/certs/
RUN mkdir -p /etc/ssl/private/
COPY cert/dev/apiwfc.dev.aclinnova.com/certificate.crt /etc/ssl/certs/nginx.crt
COPY cert/dev/apiwfc.dev.aclinnova.com/private.key /etc/ssl/private/nginx.key
COPY cert/dev/letsencrypt/3378D866F2E9775FC2A7A84C977C9AD9.txt /usr/share/nginx/html/.well-known/pki-validation/3378D866F2E9775FC2A7A84C977C9AD9.txt
COPY cert/dev/default.conf /etc/nginx/conf.d/default.conf
EXPOSE 443
CMD ["nginx", "-g", "daemon off;"]